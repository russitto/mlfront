import { h, Component } from 'preact'
import { Router } from 'preact-router'
import SimpleItem from './simple_item'
import Categories from '../../components/categories'
import style from './style'

const _l = console.log.bind(console)
const BASE = '/api/items?q='

export default class Items extends Component {
  constructor(props) {
    super(props)
    const search = location.search.substr(8)
    this.state = { search, items: [], loading: true }
    this.lastUrl = ''
  }

  // fix classic querystring routes:
  handleRoute(e) {
    const url = e.url.replace(/ /g, '%20')
    if (this.lastUrl != '' && this.lastUrl != url) {
      this.setState({search: url.substr(14)}) // .substr(14) -> 14 = /items?search=
      this.update()
    }
    this.lastUrl = url
  }

  componentDidMount() {
    this.update()
  }

  update() {
    const decSearch = decodeURI(this.state.search)
    document.title = decSearch + ' en Mercado Libre'

    this.setState({loading: true})
    const url = BASE + this.state.search
    fetch(url
    ).then(res => res.json()
    ).then(data => {
      // Categories.update
      this.setState({search: this.state.search, items: data.items, loading: false})
      this.props.updateCats(data.categories)
    })
  }

  render() {
    // document.querySelector('form input[name=search]')
    const rows = []
    for (let i = 0; i < this.state.items.length; i++) {
      if (i) {
        rows.push(<hr />)
      }
      rows.push(<SimpleItem obj={this.state.items[i]} />)
    }
    return (
      <div class="container">
        <Router onChange={this.handleRoute.bind(this)}></Router>
        <div class={style.itemlist}>
          <div class={'loading load' + this.state.loading}>Cargando…</div>
          {rows}
        </div>
      </div>
    )
  }
}
