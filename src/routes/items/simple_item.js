import { h } from 'preact'
import { Link } from 'preact-router/match'
import style from './style'

const SimpleItem = params => {
  const decimalClass = 'val' + params.obj.price.decimals
  const href = '/items/' + params.obj.id
  const freeShipClass = style['free-ship-' + params.obj.free_shipping]
  const freeShipText = params.obj.free_shipping? 'Envío gratuito': ''
  return (
    <div class={style.item}>
      <Link href={href}>
        <img src={params.obj.picture} alt="thumbnail" class={style.thumbnail} />
      </Link>
      <div class={style.description}>
        <div class={style.price}>
          <div class={style.number}>
            {getPrice(params.obj.price)}
            <sup class={style[decimalClass]}>{params.obj.price.decimals}</sup>
          </div>
          <div title={freeShipText} class={style.shipping + ' ' +  freeShipClass}></div>
        </div>
        <div class={style.title}>
          <Link href={href}>{params.obj.title}</Link>
        </div>
      </div>
      <div class={style.city}>
        {params.obj.city}
      </div>
    </div>
  )
}

export default SimpleItem

function getPrice(price) {
  return '$ ' + price.amount.toString().replace(/\B(?=(\d{3})+(?!\d))/g, '.')
}
