import { h, Component } from 'preact'
import style from './style'
import Categories from '../../components/categories'

const _l = console.log.bind(console)
const BASE = '/api/items/'

export default class Item extends Component {
  conditionLabels = {
    "new": "nuevo",
    "used": "usado"
  }

  constructor(props) {
    super(props)
    this.state = { data: {}, loading: true, id: props.id }
  }

  componentDidMount() {
    this.update()
  }

  update() {
    this.setState({loading: true})
    const url = BASE + this.state.id
    fetch(url
    ).then(res => res.json()
    ).then(data => {
      // _l(data)
      this.setState({data, loading: false})
    })
  }

  render() {
    if (this.state.loading) {
      return (
        <div class="container">
          <div class="loading loadtrue">Cargando…</div>
        </div>
      )
    }

    const item = this.state.data.item
    document.title = item.title + ' en Mercado Libre'

    const decimalClass = 'val' + item.price.decimals
    const freeShipClass = style['free-ship-' + item.free_shipping]
    const freeShipText = item.free_shipping? 'Envío gratuito': ''
    return (
      <div class="container">
        <div class={style.item}>
          <div class={style.col0}>
            <a href={item.picture} target="mlpic" title={item.title}>
              <img src={item.picture} alt={item.title} class={style.picture} />
            </a>
            <h2 class={style.descrTit}>Descripción del producto</h2>
            <p class={style.description}>{item.description}</p>
          </div>
          <div class={style.col1}>
            <div>
              <span class={style.condition}>{this.conditionLabels[item.condition]} - </span>
              <span class={style.sold}>{item.sold_quantity} vendidos</span>
              <span title={freeShipText} class={style.shipping + ' ' +  freeShipClass}></span>
            </div>
            <h2 class={style.title}>{item.title}</h2>
            <div class={style.price}>
              {getPrice(item.price)}
              <sup class={style[decimalClass]}>{item.price.decimals}</sup>
            </div>
            <button class={style.buyit}>Comprar</button>
          </div>
        </div>
      </div>
    )
  }
}

function getPrice(price) {
  const millons = parseInt(price.amount / 1000000)
  const thousands = parseInt((price.amount % 1000000) / 1000)
  let sprice = price.amount % 1000
  if (thousands) {
    sprice = thousands + '.' + sprice
  }
  if (millons) {
    sprice = millons + '.' + sprice
  }
  return '$ ' + sprice
}
