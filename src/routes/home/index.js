import { h, Component } from 'preact'
import style from './style'

export default class Home extends Component {
  render() {
    // document.title = 'Mercado Libre'

    const links = [
      'https://russitto.com/',
      'https://gitlab.com/russitto',
      '/items?search=apple ipod'
    ].map(li => (<li><a href={li}>{li}</a></li>))

    return (
      <div class={style.home}>
        <h1>Cliente Frontend por Matias Russitto</h1>
        <ul>{links}</ul>
      </div>
    )
  }
}
