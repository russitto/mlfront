import { h, Component } from 'preact'
import style from './style'

const _l = console.log.bind(console)
export default class Categories extends Component {
  render() {
    const cats = this.props.items.map(cat => (<li>{cat}</li>))
    return(
      <div class={style.catcontainer}>
        <ul class={style.categories + ' ' + style['size' + cats.length]}>{cats}</ul>
      </div>
    )
  }
}
