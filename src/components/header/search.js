import { h, Component } from 'preact'
import { route } from 'preact-router';
import style from './style'

export default class Search extends Component {
  constructor(props) {
    super(props)
    this.submit = this.submit.bind(this)
  }

  state = {
    // @TODO set value as safe parsed location.search
    value: '' // decodeURI(location.search.substr(8)) // substr(8) removes '?search='
  }

  submit(ev) {
    ev.preventDefault()
    const value = ev.target.querySelector('input[name=search]').value
    const url = '/items?search=' + value
    this.setState({value})
    route(url)
  }

  render() {
    return (
      <form action="/items" onSubmit={this.submit} class={style.search}>
        <input type="text" id="main-search" placeholder="Nunca dejes de buscar" name="search" value={this.state.value} autofocus/>
        <button type="submit">Buscar</button>
      </form>
    )
  }
}
