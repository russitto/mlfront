import { h, Component } from 'preact'
import { Link } from 'preact-router/match'
import Search from './search'
import style from './style'

export default class Header extends Component {
  focus(ev) {
    ev.target.parentNode.querySelector('input[name=search]').focus()
  }

  render() {
    return (
      <header class={style.header}>
        <div class={style.container}>
          <Link class={style.logo} onclick={this.focus} href="/">Inicio</Link>
          <Search/>
        </div>
      </header>
    )
  }
}
