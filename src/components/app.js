import { h, Component } from 'preact'
import { Router } from 'preact-router'

import Header from './header'
import Categories from './categories'
import Home from '../routes/home'
import Items from '../routes/items'
import Item from '../routes/item'

const _l = console.log.bind(console)
export default class App extends Component {
  constructor(props) {
    super(props)
    this.state = { categories: [] }
    this.itemsUpdCats = this.itemsUpdCats.bind(this)
  }
	
	handleRoute = e => {
    // _l('main router', e.url)
		this.currentUrl = e.url
    // if (e.url == '/') {
    //   this.setState({categories: []})
    // }
	}

  itemsUpdCats(cats) {
    this.setState({ categories: cats })
  }

	render() {
		return (
			<div id="app">
				<Header />
				<Categories items={this.state.categories} />
        <div class="container">
          <Router onChange={this.handleRoute}>
            <Home path="/" updateCats={this.itemsUpdCats} />
            <Item path="/items/:id" />
            <Items path="/items" updateCats={this.itemsUpdCats} />
          </Router>
        </div>
			</div>
		)
    // <Profile path="/profile/:user" />
	}
}
