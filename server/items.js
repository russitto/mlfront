const request = require('request')

const BASE_SEARCH = 'https://api.mercadolibre.com/sites/MLA/search?limit=4&q='
const BASE_ITEM = 'https://api.mercadolibre.com/items/'
const author = {
  name: "Matias",
  lastname: "Russitto"
}

module.exports = { search, element }

function search(req, res) {
  _l('items.search', req.url)
  setHeaders(res)
  const q = req.query.q || ''

  request(BASE_SEARCH + q, (err, response, body) => {
    res.status(response.statusCode)
    if (err) {
      _e(err)
      res.send(err)
      return
    }

    let json
    try {
      json = JSON.parse(body)
    } catch (e) {
      _e(err)
      res.send(err)
      return
    }

    let categories = [], i
    for (i = 0; i < json.filters.length && json.filters[i].id != 'category'; i++);
    // @TODO verify simple or multiple category paths
    if (i < json.filters.length && json.filters[i].values.length) {
      // filter: category, first path
      categories = json.filters[i].values[0].path_from_root.map(cat => cat.name)
    }
    let items = json.results.map(result => {
      const amount = parseInt(result.price)
      const decimals = getDecimals(result.price)
      return {
        id: result.id,
        title: result.title,
        price: {
          currency: result.currency_id,
          amount,
          decimals 
        },
        picture: result.thumbnail,
        condition: result.condition,
        free_shipping: result.shipping.free_shipping,
        city: result.seller_address.city.name
      }
    })

    res.send({ author, categories, items })
  })
}

function element(req, res) {
  const id = req.params.id
  _l('items.element id:', id, req.url)
  setHeaders(res)
  Promise.all([
    get(BASE_ITEM + id),
    get(BASE_ITEM + id + '/description')
  ]).then(([itemObj, descObj]) => {
    const item_err      = itemObj.err
    const item_response = itemObj.response
    const item_body     = itemObj.body
    const desc_err      = descObj.err
    const desc_response = descObj.response
    const desc_body     = descObj.body
    if (item_err) {
      _e(item_err)
      res.send(item_err)
      return
    }
    if (desc_err) {
      _e(desc_err)
      res.send(desc_err)
      return
    }

    let item_json, desc_json
    try {
      item_json = JSON.parse(item_body)
      desc_json = JSON.parse(desc_body)
    } catch (err) {
      _e(err)
      res.send(err)
      return
    }

    const amount = parseInt(item_json.price)
    const decimals = getDecimals(item_json.price)
    let picture = item_json.thumbnail
    if (item_json.pictures && item_json.pictures.length) {
      picture = item_json.pictures[0].secure_url
    }
    const item = {
      id: item_json.id,
      title: item_json.title,
      price: {
        currency: item_json.currency_id,
        amount, decimals
      },
      picture,
      condition: item_json.condition,
      free_shipping: item_json.shipping.free_shipping,
      sold_quantity: item_json.sold_quantity,
      description: desc_json.plain_text
    }
    res.send({ author, item })
  })
}

function _l(...args) {
  console.log(new Date().toISOString() , args)
}

function _e(...args) {
  console.error(new Date().toISOString() , args)
}

function getDecimals(num) {
  if (num == parseInt(num)) {
    return 0
  }
  return parseInt(num.toString().replace(/\d+\./, ''))
}

// request.get @ promise
function get(url) {
  return new Promise(resolve => {
    request(url, (err, response, body) => {
      resolve({err, response, body})
    })
  })
}

function setHeaders(res) {
  res.setHeader('Content-Type', 'application/json')
  // res.setHeader('access-control-allow-origin', '*')
  // res.setHeader('access-control-allow-headers', 'Origin, X-Requested-With, Content-Type, Accept, Range')
}
