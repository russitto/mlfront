// @TODO http2 requests
// const http2 = require('http2')
// const fs = require('fs')
const express = require('express')
const items = require('./items')

const PORT = process.env.PORT || 8080
const _l = console.log.bind(console)

const app = express()

app.use(express.static(__dirname + '/../build'))
app.get('/api/items', items.search)
// app.get('/api/items/:id', (req, res) => {
app.get('/api/items/:id', items.element)

// const server = http2.createSecureServer(
//   fs.readFileSync(__dirname + '/certs/http2-express-server.key'),
//   fs.readFileSync(__dirname + '/certs/http2-express-server.crt'),
//   app)
// server.listen(PORT, () => {
app.listen(PORT, () => {
  _l(`Listening at ${PORT}`)
})
